
package institute;

import java.util.List;

/**
 *
 * @author mitpa
 */
public class Department 
{
    String name;
    private List<Student> students;

    public Department(String name,List<Student> students) {
        this.name = name;
        this.students = students;
    }
    public List<Student> getStudent()
    {
        return students;
    }
    
    
}
