
package institute;
import java.util.List;

/**
 *
 * @author mitpa
 */
public class Institute 
{
   String instituteName;
   private List<Department> departments;
   
   public Institute(String instituteName,List<Department departments)
   
   {
       this.instituteName = instituteName;
       this.departments = departments;
       
   }
   public int getTotalStudentsInInstitute()
   {
       int noOfStduents = 0;
       List<Student> students;
       for(Department dept : departments)
       {
           students = dept.getStudent();
           for(Student s : students)
           {
               noOfStudents++;
           }
       }
       return noOfStudents;
   }
    
}
